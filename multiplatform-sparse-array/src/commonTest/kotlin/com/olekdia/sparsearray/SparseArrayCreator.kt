package com.olekdia.sparsearray

object SparseArrayCreator {

    fun createIntToStringSparseArray(): IntSparseArray<String> =
        IntSparseArray<String>().apply {
            put(0, "str1")
            put(1, "str2")
            put(2, "str2")
            put(3, "str3")
            put(4, "str3")

            put(5, "str4")
            put(6, "str5")
            put(7, "str5")
            put(8, "str5")
            put(9, "str6")
        }

    fun createIntToStringNullableSparseArray(): IntSparseArray<String?> =
        IntSparseArray<String?>().apply {
            put(0, "str1")
            put(1, "str2")
            put(2, "str2")
            put(3, "str3")
            put(4, null)

            put(5, "str4")
            put(6, "str5")
            put(7, "str5")
            put(8, null)
            put(9, "str6")
        }

    fun createLongToStringSparseArray(): LongSparseArray<String> =
        LongSparseArray<String>().apply {
            put(0L, "str1")
            put(1L, "str2")
            put(2L, "str2")
            put(3L, "str3")
            put(4L, "str3")

            put(5L, "str4")
            put(6L, "str5")
            put(7L, "str5")
            put(8L, "str5")
            put(9L, "str6")
        }

    fun createLongToStringNullableSparseArray(): LongSparseArray<String?> =
        LongSparseArray<String?>().apply {
            put(0L, "str1")
            put(1L, "str2")
            put(2L, "str2")
            put(3L, "str3")
            put(4L, null)

            put(5L, "str4")
            put(6L, "str5")
            put(7L, "str5")
            put(8L, null)
            put(9L, "str6")
        }

    fun createIntToBooleanSparseArray(): IntToBooleanSparseArray =
        IntToBooleanSparseArray().apply {
            put(0, true)
            put(1, false)
            put(2, false)
            put(3, true)
            put(4, false)

            put(5, false)
            put(6, true)
            put(7, true)
            put(8, false)
            put(9, false)
        }

    fun createLongToBooleanSparseArray(): LongToBooleanSparseArray =
        LongToBooleanSparseArray().apply {
            put(0L, true)
            put(1L, false)
            put(2L, false)
            put(3L, true)
            put(4L, false)

            put(5L, false)
            put(6L, true)
            put(7L, true)
            put(8L, false)
            put(9L, false)
        }

    fun createIntToIntSparseArray(): IntToIntSparseArray =
        IntToIntSparseArray().apply {
            put(0, 1)
            put(1, 2)
            put(2, 2)
            put(3, 3)
            put(4, 3)

            put(5, 4)
            put(6, 5)
            put(7, 5)
            put(8, 5)
            put(9, 6)
        }

    fun createLongToLongSparseArray(): LongToLongSparseArray =
        LongToLongSparseArray().apply {
            put(0L, 1L)
            put(1L, 2L)
            put(2L, 2L)
            put(3L, 3L)
            put(4L, 3L)

            put(5L, 4L)
            put(6L, 5L)
            put(7L, 5L)
            put(8L, 5L)
            put(9L, 6L)
        }
}