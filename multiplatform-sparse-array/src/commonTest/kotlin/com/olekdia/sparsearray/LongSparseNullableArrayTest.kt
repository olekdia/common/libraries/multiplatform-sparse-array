package com.olekdia.sparsearray

import com.olekdia.common.INVALID
import com.olekdia.common.extensions.getRandom
import kotlin.test.*

class LongSparseNullableArrayTest {

    lateinit var sparseArray: LongSparseArray<String?>

    @BeforeTest
    fun setup() {
        sparseArray = SparseArrayCreator.createLongToStringNullableSparseArray()
    }

//--------------------------------------------------------------------------------------------------
//  Map methods
//--------------------------------------------------------------------------------------------------

    @Test
    fun size_filledArray_valueIsValid() {
        assertEquals(10, sparseArray.size)
    }

    @Test
    fun size_filledArray_clear_valueIsValid() {
        sparseArray.clear()
        assertEquals(0, sparseArray.size)
    }

    @Test
    fun isEmpty_filledArray_returnFalse() {
        assertFalse(sparseArray.isEmpty())
    }

    @Test
    fun isEmpty_filledArray_clear_returnTrue() {
        sparseArray.clear()
        assertTrue(sparseArray.isEmpty())
    }

    @Test
    fun containsKey_withValidKeys_returnTrue() {
        assertTrue(sparseArray.containsKey(0L))
        assertTrue(sparseArray.containsKey(5L))
        assertTrue(sparseArray.containsKey(9L))
    }

    @Test
    fun containsKey_withInvalidKeys_returnFalse() {
        assertFalse(sparseArray.containsKey(-1L))
        assertFalse(sparseArray.containsKey(10L))
    }

    @Test
    fun containsValue_withValidValues_returnTrue() {
        assertTrue(sparseArray.containsValue(null))
        assertTrue(sparseArray.containsValue("str1"))
        assertTrue(sparseArray.containsValue("str6"))
    }

    @Test
    fun containsValue_withInvalidValues_returnFalse() {
        assertFalse(sparseArray.containsValue("str0"))
        assertFalse(sparseArray.containsValue("str7"))
    }

    @Test
    fun get_withValidKeys_returnValidValue() {
        assertEquals("str1", sparseArray[0L])
        assertNull(sparseArray[4L])
        assertEquals("str6", sparseArray[9L])
    }

    @Test
    fun get_withInvalidKeys_returnNull() {
        assertNull(sparseArray[-1L])
        assertNull(sparseArray[10L])
    }

    @Test
    fun get_withValidKeys_remove_returnNull() {
        assertEquals("str1", sparseArray[0L])

        sparseArray.remove(0L)

        assertNull(sparseArray[0L])
    }

    @Test
    fun getOrDefault_withValidKeys_returnValidValue() {
        assertEquals("str1", sparseArray.getOrDefault(0L, "def"))
        /**
         * This is special case. Even key 4 has value which is null, and it is perfectly fine, because
         * we have nullable map. This method will return defultValue instead
         */
        assertEquals("def", sparseArray.getOrDefault(4L, "def"))
        assertNull(sparseArray.getOrDefault(4L, null))
        assertEquals("str6", sparseArray.getOrDefault(9L, "def"))
    }

    @Test
    fun getOrDefault_withInvalidKeys_returnDefaultValue() {
        assertEquals("def", sparseArray.getOrDefault(-1L, "def"))
        assertNull(sparseArray.getOrDefault(10L, null))
    }

    @Test
    fun keys_containAllNecessaryValues() {
        val keys: Set<Long> = sparseArray.keys

        assertEquals(10, keys.size)

        for (i in 0L..9L) {
            assertTrue(keys.contains(i))
        }
    }

    @Test
    fun keys_clear_returnEmptySet() {
        sparseArray.clear()

        val keys: Set<Long> = sparseArray.keys

        assertEquals(0, keys.size)
    }

    @Test
    fun values_containAllNecessaryValues() {
        val values: List<String?>? = sparseArray.values

        assertNotNull(values)

        assertEquals(10, values.size)

        assertTrue(values.contains("str1"))
        assertTrue(values.contains("str2"))
        assertTrue(values.contains("str3"))
        assertTrue(values.contains("str4"))
        assertTrue(values.contains("str5"))
        assertTrue(values.contains("str6"))
        assertTrue(values.contains(null))
    }

    @Test
    fun values_clear_returnEmptyList() {
        sparseArray.clear()

        val values: List<String?>? = sparseArray.values

        assertNotNull(values)

        assertEquals(0, values.size)
    }

    @Test
    fun entries_containAllNecessaryValues() {
        val entries: MutableSet<MutableMap.MutableEntry<Long, String?>> = sparseArray.entries

        assertEquals(10, entries.size)

        assertEquals(0L, entries.elementAt(0).key)
        assertEquals("str1", entries.elementAt(0).value)

        assertEquals(1L, entries.elementAt(1).key)
        assertEquals("str2", entries.elementAt(1).value)

        assertEquals(2L, entries.elementAt(2).key)
        assertEquals("str2", entries.elementAt(2).value)

        assertEquals(3L, entries.elementAt(3).key)
        assertEquals("str3", entries.elementAt(3).value)

        assertEquals(4L, entries.elementAt(4).key)
        assertEquals(null, entries.elementAt(4).value)

        assertEquals(5L, entries.elementAt(5).key)
        assertEquals("str4", entries.elementAt(5).value)

        assertEquals(6L, entries.elementAt(6).key)
        assertEquals("str5", entries.elementAt(6).value)

        assertEquals(7L, entries.elementAt(7).key)
        assertEquals("str5", entries.elementAt(7).value)

        assertEquals(8L, entries.elementAt(8).key)
        assertEquals(null, entries.elementAt(8).value)

        assertEquals(9L, entries.elementAt(9).key)
        assertEquals("str6", entries.elementAt(9).value)
    }

    @Test
    fun entries_clear_returnEmptySet() {
        sparseArray.clear()

        val entries: MutableSet<MutableMap.MutableEntry<Long, String?>> = sparseArray.entries

        assertEquals(0, entries.size)
    }

//--------------------------------------------------------------------------------------------------
//  MutableMap methods
//--------------------------------------------------------------------------------------------------

    @Test
    fun put_validKey_valueChanged_returnOldValue() {
        assertEquals(10, sparseArray.size)
        assertEquals("str1", sparseArray[0L])

        assertEquals("str1", sparseArray.put(0L, "new str"))

        assertEquals(10, sparseArray.size)
        assertEquals("new str", sparseArray[0L])
    }

    @Test
    fun put_invalidKey_valueAdded_returnNull() {
        assertEquals(10, sparseArray.size)
        assertEquals("str1", sparseArray[0L])

        assertNull(sparseArray.put(100L, "new str"))

        assertEquals(11, sparseArray.size)
        assertEquals("new str", sparseArray[100L])
    }

    @Test
    fun put_thousandValues_valuesAdded() {
        sparseArray = LongSparseArray()

        for (i in 0..1000) {
            sparseArray.put(i.toLong(), if (i % 100 == 0) null else "str$i")

            if (i == 10) {
                sparseArray.removeAt(0)
            }
        }

        assertEquals(1000, sparseArray.size)
    }

    @Test
    fun remove_byKey_validKey_valueRemoved_returnOldValue() {
        assertEquals(10, sparseArray.size)
        assertEquals("str1", sparseArray[0L])

        assertEquals("str1", sparseArray.remove(0L))

        assertEquals(9, sparseArray.size)
        assertNull(sparseArray[0L])
    }

    @Test
    fun remove_byKey_validKey_multiple_valueRemoved_returnOldValue() {
        assertEquals(10, sparseArray.size)

        assertEquals("str1", sparseArray[0L])
        assertEquals("str2", sparseArray[1L])
        assertEquals("str2", sparseArray[2L])
        assertEquals("str3", sparseArray[3L])
        assertNull(sparseArray[4L])

        assertEquals("str4", sparseArray[5L])
        assertEquals("str5", sparseArray[6L])
        assertEquals("str5", sparseArray[7L])
        assertNull(sparseArray[8L])
        assertEquals("str6", sparseArray[9L])

        assertEquals("str1", sparseArray.remove(0L))
        assertNull(sparseArray.remove(4L))
        assertEquals("str4", sparseArray.remove(5L))

        assertEquals(7, sparseArray.size)

        assertNull(sparseArray[0L])
        assertEquals("str2", sparseArray[1L])
        assertEquals("str2", sparseArray[2L])
        assertEquals("str3", sparseArray[3L])
        assertNull(sparseArray[4L])

        assertNull(sparseArray[5L])
        assertEquals("str5", sparseArray[6L])
        assertEquals("str5", sparseArray[7L])
        assertNull(sparseArray[8L])
        assertEquals("str6", sparseArray[9L])
    }

    @Test
    fun remove_byKey_invalidKey_nothingChanged_returnNull() {
        assertEquals(10, sparseArray.size)
        assertEquals("str1", sparseArray[0L])

        assertNull(sparseArray.remove(100L))

        assertEquals(10, sparseArray.size)
    }

    @Test
    fun remove_byKeyAndValue_invalidKey_invalidValue_nothingChanged_returnFalse() {
        assertEquals(10, sparseArray.size)
        assertEquals("str1", sparseArray[0L])

        assertFalse(sparseArray.remove(100L, "str100"))

        assertEquals(10, sparseArray.size)
    }

    @Test
    fun remove_byKeyAndValue_invalidKey_validValue_nothingChanged_returnFalse() {
        assertEquals(10, sparseArray.size)
        assertEquals("str1", sparseArray[0L])

        assertFalse(sparseArray.remove(100L, "str1"))

        assertEquals(10, sparseArray.size)
    }

    @Test
    fun remove_byKeyAndValue_validKey_invalidValue_nothingChanged_returnFalse() {
        assertEquals(10, sparseArray.size)
        assertEquals("str1", sparseArray[0L])

        assertFalse(sparseArray.remove(0L, "str100"))

        assertEquals(10, sparseArray.size)
    }

    @Test
    fun remove_byKeyAndValue_validKey_validValue_valueRemoved_returnTrue() {
        assertEquals(10, sparseArray.size)
        assertEquals("str1", sparseArray[0L])

        assertTrue(sparseArray.remove(0L, "str1"))

        assertEquals(9, sparseArray.size)
        assertNull(sparseArray[0L])
    }

    @Test
    fun pullAll_valuesAppearInSparseArray() {
        val newMap: Map<Long, String> = mapOf(
            Pair(10L, "str10"),
            Pair(11L, "str11"),

            Pair(0L, "new str")
        )

        sparseArray.putAll(newMap)

        val entries: MutableSet<MutableMap.MutableEntry<Long, String?>> = sparseArray.entries

        assertEquals(12, entries.size)

        assertEquals(0L, entries.elementAt(0).key)
        assertEquals("new str", entries.elementAt(0).value)

        assertEquals(1L, entries.elementAt(1).key)
        assertEquals("str2", entries.elementAt(1).value)

        assertEquals(2L, entries.elementAt(2).key)
        assertEquals("str2", entries.elementAt(2).value)

        assertEquals(3L, entries.elementAt(3).key)
        assertEquals("str3", entries.elementAt(3).value)

        assertEquals(4L, entries.elementAt(4).key)
        assertEquals(null, entries.elementAt(4).value)

        assertEquals(5L, entries.elementAt(5).key)
        assertEquals("str4", entries.elementAt(5).value)

        assertEquals(6L, entries.elementAt(6).key)
        assertEquals("str5", entries.elementAt(6).value)

        assertEquals(7L, entries.elementAt(7).key)
        assertEquals("str5", entries.elementAt(7).value)

        assertEquals(8L, entries.elementAt(8).key)
        assertEquals(null, entries.elementAt(8).value)

        assertEquals(9L, entries.elementAt(9).key)
        assertEquals("str6", entries.elementAt(9).value)

        assertEquals(10L, entries.elementAt(10).key)
        assertEquals("str10", entries.elementAt(10).value)

        assertEquals(11L, entries.elementAt(11).key)
        assertEquals("str11", entries.elementAt(11).value)
    }

    @Test
    fun clear_sparseArrayIsEmpty() {
        assertFalse(sparseArray.isEmpty())

        sparseArray.clear()

        assertTrue(sparseArray.isEmpty())
    }

//--------------------------------------------------------------------------------------------------
//  Other methods
//--------------------------------------------------------------------------------------------------

    @Test
    fun removeAt_validIndex_valueRemoved() {
        assertEquals(10, sparseArray.size)
        assertEquals("str1", sparseArray[0L])

        sparseArray.removeAt(0)

        assertEquals(9, sparseArray.size)
        assertNull(sparseArray[0L])
    }

    @Test
    fun removeAt_invalidIndex_nothingChanged() {
        assertEquals(10, sparseArray.size)
        assertEquals("str1", sparseArray[0L])

        sparseArray.removeAt(-1)
        assertEquals(10, sparseArray.size)

        sparseArray.removeAt(sparseArray.size)
        assertEquals(10, sparseArray.size)
    }

    @Test
    fun removeAtRange_invalidIndex_invalidSize_nothingChanged() {
        assertEquals(10, sparseArray.size)

        sparseArray.removeAtRange(-1, -1)

        assertEquals(10, sparseArray.size)
    }

    @Test
    fun removeAtRange_invalidIndex_tooBigSize_nothingChanged() {
        assertEquals(10, sparseArray.size)

        sparseArray.removeAtRange(-1, 100)

        assertEquals(10, sparseArray.size)
    }

    @Test
    fun removeAtRange_invalidIndex_validSize_nothingChanged() {
        assertEquals(10, sparseArray.size)

        sparseArray.removeAtRange(-1, 2)

        assertEquals(10, sparseArray.size)
    }

    @Test
    fun removeAtRange_validIndex_invalidSize_nothingChanged() {
        assertEquals(10, sparseArray.size)

        sparseArray.removeAtRange(0, -1)

        assertEquals(10, sparseArray.size)
    }

    @Test
    fun removeAtRange_validIndex_tooBigSize_sizeNormalized_valuesRemoved() {
        assertEquals(10, sparseArray.size)

        sparseArray.removeAtRange(5, 100)

        assertEquals(5, sparseArray.size)

        assertEquals("str1", sparseArray[0L])
        assertEquals("str2", sparseArray[1L])
        assertEquals("str2", sparseArray[2L])
        assertEquals("str3", sparseArray[3L])
        assertNull(sparseArray[4L])
    }

    @Test
    fun removeAtRange_validIndex_validSize_valuesRemoved() {
        assertEquals(10, sparseArray.size)

        sparseArray.removeAtRange(5, 5)

        assertEquals(5, sparseArray.size)

        assertEquals("str1", sparseArray[0L])
        assertEquals("str2", sparseArray[1L])
        assertEquals("str2", sparseArray[2L])
        assertEquals("str3", sparseArray[3L])
        assertNull(sparseArray[4L])
    }

    @Test
    fun removeAtRange_validIndex_validSize_fromCenter_valuesRemoved() {
        assertEquals(10, sparseArray.size)

        sparseArray.removeAtRange(5, 2)

        assertEquals(8, sparseArray.size)

        val values: MutableList<String?> = sparseArray.values

        assertEquals(8, values.size)

        assertEquals("str1", values[0])
        assertEquals("str2", values[1])
        assertEquals("str2", values[2])
        assertEquals("str3", values[3])
        assertNull(values[4])

        assertEquals("str5", values[5])
        assertNull(values[6])
        assertEquals("str6", values[7])
    }

    @Test
    fun keyAt_invalidIndex_returnNull() {
        assertNull(sparseArray.keyAt(-1))
        assertNull(sparseArray.keyAt(sparseArray.size))
    }

    @Test
    fun keyAt_validIndex_valueIsValid() {
        assertEquals(0L, sparseArray.keyAt(0))
        assertEquals(9L, sparseArray.keyAt(9))
    }

    @Test
    fun keyAt_validIndex_remove_valueIsValid_lastIndexReturnNull() {
        assertEquals(0L, sparseArray.keyAt(0))
        assertEquals(9L, sparseArray.keyAt(9))

        sparseArray.removeAt(5)

        assertEquals(0L, sparseArray.keyAt(0))
        assertEquals(9L, sparseArray.keyAt(8))
        assertNull(sparseArray.keyAt(9))
    }

    @Test
    fun valueAt_invalidIndex_returnNull() {
        assertNull(sparseArray.valueAt(-1))
        assertNull(sparseArray.valueAt(sparseArray.size))
    }

    @Test
    fun valueAt_validIndex_valueIsValid() {
        assertEquals("str1", sparseArray.valueAt(0))
        assertNull(sparseArray.valueAt(4))
        assertEquals("str6", sparseArray.valueAt(9))
    }

    @Test
    fun valueAt_validIndex_remove_valueIsValid_lastIndexReturnNull() {
        assertEquals("str1", sparseArray.valueAt(0))
        assertNull(sparseArray.valueAt(4))
        assertEquals("str6", sparseArray.valueAt(9))

        sparseArray.removeAt(5)

        assertEquals("str1", sparseArray.valueAt(0))
        assertNull(sparseArray.valueAt(4))
        assertEquals("str6", sparseArray.valueAt(8))
        assertNull(sparseArray.valueAt(9))
    }

    @Test
    fun setKeyAt_invalidIndex_returnFalse_keysNotChanged() {
        assertEquals(0L, sparseArray.keyAt(0))
        assertEquals(1L, sparseArray.keyAt(1))
        assertEquals(2L, sparseArray.keyAt(2))
        assertEquals(3L, sparseArray.keyAt(3))
        assertEquals(4L, sparseArray.keyAt(4))

        assertEquals(5L, sparseArray.keyAt(5))
        assertEquals(6L, sparseArray.keyAt(6))
        assertEquals(7L, sparseArray.keyAt(7))
        assertEquals(8L, sparseArray.keyAt(8))
        assertEquals(9L, sparseArray.keyAt(9))

        assertFalse(sparseArray.setKeyAt(-1, 100L))
        assertFalse(sparseArray.setKeyAt(sparseArray.size, 100L))

        assertEquals(0L, sparseArray.keyAt(0))
        assertEquals(1L, sparseArray.keyAt(1))
        assertEquals(2L, sparseArray.keyAt(2))
        assertEquals(3L, sparseArray.keyAt(3))
        assertEquals(4L, sparseArray.keyAt(4))

        assertEquals(5L, sparseArray.keyAt(5))
        assertEquals(6L, sparseArray.keyAt(6))
        assertEquals(7L, sparseArray.keyAt(7))
        assertEquals(8L, sparseArray.keyAt(8))
        assertEquals(9L, sparseArray.keyAt(9))
    }

    @Test
    fun setKeyAt_validIndex_returnTrue_keysChanged() {
        assertEquals(0L, sparseArray.keyAt(0))
        assertEquals(1L, sparseArray.keyAt(1))
        assertEquals(2L, sparseArray.keyAt(2))
        assertEquals(3L, sparseArray.keyAt(3))
        assertEquals(4L, sparseArray.keyAt(4))

        assertEquals(5L, sparseArray.keyAt(5))
        assertEquals(6L, sparseArray.keyAt(6))
        assertEquals(7L, sparseArray.keyAt(7))
        assertEquals(8L, sparseArray.keyAt(8))
        assertEquals(9L, sparseArray.keyAt(9))

        assertTrue(sparseArray.setKeyAt(0, 100L))
        assertTrue(sparseArray.setKeyAt(9, 200L))

        assertEquals(100L, sparseArray.keyAt(0))
        assertEquals(1L, sparseArray.keyAt(1))
        assertEquals(2L, sparseArray.keyAt(2))
        assertEquals(3L, sparseArray.keyAt(3))
        assertEquals(4L, sparseArray.keyAt(4))

        assertEquals(5L, sparseArray.keyAt(5))
        assertEquals(6L, sparseArray.keyAt(6))
        assertEquals(7L, sparseArray.keyAt(7))
        assertEquals(8L, sparseArray.keyAt(8))
        assertEquals(200L, sparseArray.keyAt(9))
    }

    @Test
    fun setKeyAt_validIndex_remove_returnTrue_keysChanged_lastIndexReturnFalse() {
        assertEquals(0L, sparseArray.keyAt(0))
        assertEquals(1L, sparseArray.keyAt(1))
        assertEquals(2L, sparseArray.keyAt(2))
        assertEquals(3L, sparseArray.keyAt(3))
        assertEquals(4L, sparseArray.keyAt(4))

        assertEquals(5L, sparseArray.keyAt(5))
        assertEquals(6L, sparseArray.keyAt(6))
        assertEquals(7L, sparseArray.keyAt(7))
        assertEquals(8L, sparseArray.keyAt(8))
        assertEquals(9L, sparseArray.keyAt(9))

        sparseArray.removeAt(5)

        assertTrue(sparseArray.setKeyAt(0, 100L))
        assertTrue(sparseArray.setKeyAt(8, 200L))
        assertFalse(sparseArray.setKeyAt(9, 300L))

        assertEquals(100L, sparseArray.keyAt(0))
        assertEquals(1L, sparseArray.keyAt(1))
        assertEquals(2L, sparseArray.keyAt(2))
        assertEquals(3L, sparseArray.keyAt(3))
        assertEquals(4L, sparseArray.keyAt(4))

        assertEquals(6L, sparseArray.keyAt(5))
        assertEquals(7L, sparseArray.keyAt(6))
        assertEquals(8L, sparseArray.keyAt(7))
        assertEquals(200L, sparseArray.keyAt(8))

        assertNull(sparseArray.keyAt(9))
    }

    @Test
    fun indexOfKey_invalidKey_returnInvalidValue() {
        assertTrue(sparseArray.indexOfKey(-1L) < 0)
        assertTrue(sparseArray.indexOfKey(100L) < 0)
    }

    @Test
    fun indexOfKey_validKey_valueIsValid() {
        assertEquals(0, sparseArray.indexOfKey(0L))
        assertEquals(8, sparseArray.indexOfKey(8L))
    }

    @Test
    fun indexOfSameValue_invalidValue_returnInvalid() {
        var str: String = buildString { }
        str += "invalid str"

        assertEquals(INVALID, sparseArray.indexOfSameValue(str))
    }

    @Test
    fun indexOfSameValue_equalValue_returnInvalid() {
        var str: String = buildString { }
        str += "str1"

        assertEquals(INVALID, sparseArray.indexOfSameValue(str))
    }

    @Test
    fun indexOfSameValue_sameValue_valueIsValid() {
        var str: String = buildString { }
        str += "str1"

        sparseArray.put(10L, str)
        assertEquals(10, sparseArray.indexOfSameValue(str))
    }

    @Test
    fun indexOfSameValue_forClassThatImplEquals_valueIsValid() {
        val sparseArray: LongSparseArray<ClassForTesting> =
            LongSparseArray<ClassForTesting>().apply {
                put(0L, ClassForTesting(1, "one"))
                put(1L, ClassForTesting(2, "two"))
            }

        val array = ClassForTesting(1, "one")

        assertEquals(INVALID, sparseArray.indexOfSameValue(array))

        sparseArray.put(2L, array)
        assertEquals(2, sparseArray.indexOfSameValue(array))
    }

    @Test
    fun indexOfEqualValue_invalidValue_returnInvalid() {
        var str: String = buildString { }
        str += "str"

        assertEquals(INVALID, sparseArray.indexOfEqualValue(str))
    }

    @Test
    fun indexOfEqualValue_sameOrEqualValue_valueIsValid() {
        var str: String = buildString { }
        str += "str1"

        assertEquals(0, sparseArray.indexOfEqualValue(str))
    }

    @Test
    fun indexOfEqualValue_forClassThatImplEquals_valueIsValid() {
        val sparseArray: LongSparseArray<ClassForTesting> =
            LongSparseArray<ClassForTesting>().apply {
                put(0L, ClassForTesting(1, "one"))
                put(1L, ClassForTesting(2, "two"))
            }

        val array = ClassForTesting(1, "one")

        assertEquals(0, sparseArray.indexOfEqualValue(array))

        sparseArray.put(2L, array)
        assertEquals(0, sparseArray.indexOfEqualValue(array))
    }

    @Test
    fun ensureCapacity_newCapacityLessThanSize_returnInvalid() {
        assertEquals(INVALID, sparseArray.ensureCapacity(10))
    }

    @Test
    fun ensureCapacity_newCapacityBiggerThanSize_returnNewSize() {
        assertEquals(15, sparseArray.ensureCapacity(15))
    }


    @Test
    fun append_keyLessThanMaxKey_valuesAdded() {
        sparseArray.append(5L, "str4 new")
        assertEquals("str4 new", sparseArray[5L])
    }

    @Test
    fun append_keyBiggerThanMaxKey_optimize_valuesAdded() {
        sparseArray.append(100L, "str new")
        assertEquals("str new", sparseArray[100L])
        assertEquals(sparseArray.size - 1, sparseArray.indexOfKey(100L))
    }

//--------------------------------------------------------------------------------------------------
//  Iterator
//--------------------------------------------------------------------------------------------------

    @Test
    fun iterator_worksCorrect() {
        var counter = 0
        val keysSb = StringBuilder()
        val valuesSb = StringBuilder()

        val iterator: Iterator<Map.Entry<Long, String?>> = sparseArray.iterator()

        while (iterator.hasNext()) {
            val entry: Map.Entry<Long, String?> = iterator.next()

            keysSb.append(entry.key)
            valuesSb.append(entry.value)

            counter++
        }

        assertEquals(10, counter)
        assertEquals("0123456789", keysSb.toString())
        assertEquals(
            "str1str2str2str3nullstr4str5str5nullstr6",
            valuesSb.toString()
        )
    }

    @Test
    fun iterator_remove_worksCorrect() {
        assertEquals("str1", sparseArray[0L])
        assertEquals("str2", sparseArray[1L])
        assertEquals("str2", sparseArray[2L])
        assertEquals("str3", sparseArray[3L])
        assertNull(sparseArray[4L])

        assertEquals("str4", sparseArray[5L])
        assertEquals("str5", sparseArray[6L])
        assertEquals("str5", sparseArray[7L])
        assertNull(sparseArray[8L])
        assertEquals("str6", sparseArray[9L])

        val iterator: MutableIterator<Map.Entry<Long, String?>> = sparseArray.iterator()

        while (iterator.hasNext()) {
            val entry: Map.Entry<Long, String?> = iterator.next()

            if (entry.value == "str3") {
                iterator.remove()
            }
        }

        assertEquals(9, sparseArray.size)

        assertEquals("str1", sparseArray[0L])
        assertEquals("str2", sparseArray[1L])
        assertEquals("str2", sparseArray[2L])
        assertNull(sparseArray[3L])
        assertNull(sparseArray[4L])

        assertEquals("str4", sparseArray[5L])
        assertEquals("str5", sparseArray[6L])
        assertEquals("str5", sparseArray[7L])
        assertNull(sparseArray[8L])
        assertEquals("str6", sparseArray[9L])
    }

    @Test
    fun iterator_removeAll_worksCorrect() {
        val iterator: MutableIterator<Map.Entry<Long, String?>> = sparseArray.iterator()

        while (iterator.hasNext()) {
            iterator.next()
            iterator.remove()
        }

        assertEquals(0, sparseArray.size)
    }

    @Test
    fun iteratorInForLoop_worksCorrect() {
        var counter = 0
        val keysSb = StringBuilder()
        val valuesSb = StringBuilder()

        for ((key, value) in sparseArray) {
            keysSb.append(key)
            valuesSb.append(value)
            counter++
        }

        assertEquals(10, counter)
        assertEquals("0123456789", keysSb.toString())
        assertEquals(
            "str1str2str2str3nullstr4str5str5nullstr6",
            valuesSb.toString()
        )
    }

    @Test
    fun keysIterator_worksCorrect() {
        var counter = 0
        val keysSb = StringBuilder()

        val iterator: Iterator<Long> = sparseArray.keysIterator

        while (iterator.hasNext()) {
            val key: Long = iterator.next()
            keysSb.append(key)
            counter++
        }

        assertEquals(10, counter)
        assertEquals("0123456789", keysSb.toString())
    }

    @Test
    fun keysIteratorInForLoop_worksCorrect() {
        var counter = 0
        val keysSb = StringBuilder()

        for (key in sparseArray.keysIterator) {
            keysSb.append(key)
            counter++
        }

        assertEquals(10, counter)
        assertEquals("0123456789", keysSb.toString())
    }

    @Test
    fun valuesIterator_worksCorrect() {
        var counter = 0
        val valuesSb = StringBuilder()

        val iterator: Iterator<String?> = sparseArray.valuesIterator

        while (iterator.hasNext()) {
            val value: String? = iterator.next()
            valuesSb.append(value)
            counter++
        }

        assertEquals(10, counter)
        assertEquals(
            "str1str2str2str3nullstr4str5str5nullstr6",
            valuesSb.toString()
        )
    }

    @Test
    fun valuesIteratorInForLoop_worksCorrect() {
        var counter = 0
        val valuesSb = StringBuilder()

        for (value in sparseArray.valuesIterator) {
            valuesSb.append(value)
            counter++
        }

        assertEquals(10, counter)
        assertEquals(
            "str1str2str2str3nullstr4str5str5nullstr6",
            valuesSb.toString()
        )
    }

    @Test
    fun iterable_pair_sameCount() {
        val map: LongSparseArray<Long?> = LongSparseArray(0)

        for (i in 0..100) {
            map.append(
                i + 134L,
                if (i % 5 == 0) null else getRandom(0L, i * 100L)
            )
        }

        var iterableCount = 0
        var directForCount = 0
        var reverseForCount = 0
        var whileSizeCount = 0
        var whileIteratorCount = 0

        for ((key, value) in map.iterator()) {
            iterableCount++
        }

        for (i in 0..map.lastIndex) {
            directForCount++
        }

        for (i in map.lastIndex downTo 0) {
            reverseForCount++
        }

        while (whileSizeCount < map.size) {
            whileSizeCount++
        }

        val iterator = map.iterator()
        while (iterator.hasNext()) {
            iterator.next()
            whileIteratorCount++
        }

        assertEquals(iterableCount, directForCount)
        assertEquals(directForCount, reverseForCount)
        assertEquals(reverseForCount, whileSizeCount)
        assertEquals(whileSizeCount, whileIteratorCount)
    }

    @Test
    fun iterable_key_sameCount() {
        val map: LongSparseArray<Long?> = LongSparseArray(0)

        for (i in 0..100) {
            map.append(
                i + 134L,
                if (i % 5 == 0) null else getRandom(0L, i * 100L)
            )
        }

        var iterableCount = 0
        var directForCount = 0
        var reverseForCount = 0
        var whileSizeCount = 0
        var whileIteratorCount = 0

        for (key in map.keysIterator) {
            iterableCount++
        }

        for (i in 0..map.keys.size - 1) {
            directForCount++
        }

        for (i in map.keys.size - 1 downTo 0) {
            reverseForCount++
        }

        while (whileSizeCount < map.keys.size) {
            whileSizeCount++
        }

        val iterator = map.keysIterator
        while (iterator.hasNext()) {
            iterator.next()
            whileIteratorCount++
        }

        assertEquals(iterableCount, directForCount)
        assertEquals(directForCount, reverseForCount)
        assertEquals(reverseForCount, whileSizeCount)
        assertEquals(whileSizeCount, whileIteratorCount)
    }

    @Test
    fun iterable_value_sameCount() {
        val map: LongSparseArray<Long?> = LongSparseArray(0)

        for (i in 0..100) {
            map.append(
                i + 134L,
                if (i % 5 == 0) null else getRandom(0L, i * 100L)
            )
        }

        var iterableCount = 0
        var directForCount = 0
        var reverseForCount = 0
        var whileSizeCount = 0
        var whileIteratorCount = 0

        for (value in map.valuesIterator) {
            iterableCount++
        }

        for (i in 0..map.values.size - 1) {
            directForCount++
        }

        for (i in map.values.size - 1 downTo 0) {
            reverseForCount++
        }

        while (whileSizeCount < map.values.size) {
            whileSizeCount++
        }

        val iterator = map.valuesIterator
        while (iterator.hasNext()) {
            iterator.next()
            whileIteratorCount++
        }

        assertEquals(iterableCount, directForCount)
        assertEquals(directForCount, reverseForCount)
        assertEquals(reverseForCount, whileSizeCount)
        assertEquals(whileSizeCount, whileIteratorCount)
    }
}