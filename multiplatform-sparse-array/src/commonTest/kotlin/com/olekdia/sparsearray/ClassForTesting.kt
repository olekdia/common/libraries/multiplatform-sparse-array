package com.olekdia.sparsearray

class ClassForTesting(
    private var a: Int,
    private var b: String
) {
    override fun equals(other: Any?): Boolean =
        other is ClassForTesting && a == other.a && b == other.b

    override fun hashCode(): Int {
        var result = a
        result = 31 * result + b.hashCode()
        return result
    }
}