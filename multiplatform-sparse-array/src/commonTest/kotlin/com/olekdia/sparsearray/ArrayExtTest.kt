package com.olekdia.sparsearray

import kotlin.test.Test
import kotlin.test.assertEquals

class ArrayExtTest {

    @Test
    fun createIntSparseArray_lastIndexIsValid() {
        val sparseArray: IntSparseArray<String> =
            SparseArrayCreator.createIntToStringSparseArray()

        assertEquals(9, sparseArray.lastIndex)
    }

    @Test
    fun createLongSparseArray_lastIndexIsValid() {
        val sparseArray: LongSparseArray<String> =
            SparseArrayCreator.createLongToStringSparseArray()

        assertEquals(9, sparseArray.lastIndex)
    }

    @Test
    fun createLongToBooleanSparseArray_lastIndexIsValid() {
        val sparseArray: LongToBooleanSparseArray =
            SparseArrayCreator.createLongToBooleanSparseArray()

        assertEquals(9, sparseArray.lastIndex)
    }
}