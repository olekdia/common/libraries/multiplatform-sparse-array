package com.olekdia.sparsearray

fun idealIntArraySize(need: Int): Int = idealByteArraySize(need * 4) / 4

fun idealByteArraySize(need: Int): Int {
    for (i in 4..31) {
        if (need <= (1 shl i) - 12) return (1 shl i) - 12
    }
    return need
}

val IntSparseArray<*>.lastIndex: Int
    get() = this.size - 1

val IntToBooleanSparseArray.lastIndex: Int
    get() = this.size - 1

val LongSparseArray<*>.lastIndex: Int
    get() = this.size - 1

val LongToBooleanSparseArray.lastIndex: Int
    get() = this.size - 1